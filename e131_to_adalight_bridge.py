import sys
import sacn
import serial
import time
import serial.tools.list_ports

COMMON_BAUD_RATES = [
    9600, 14400, 19200, 38400, 57600, 115200, 
    230400, 250000, 460800, 500000, 750000, 921600, 1500000
]
DEFAULT_BAUD_RATE = 115200  # Default for ESP32

def list_serial_ports():
    return {port.device: port.description for port in serial.tools.list_ports.comports()}

def user_choice_port(ports):
    print("Please select a USB device:")
    for idx, (port, description) in enumerate(ports.items()):
        print(f"{idx + 1}. {port} ({description})")
    
    while True:
        try:
            choice = int(input("Enter your choice (number): "))
            if 1 <= choice <= len(ports):
                return list(ports.keys())[choice - 1]
            else:
                print("Invalid choice. Please choose again.")
        except ValueError:
            print("Please enter a valid number.")

def select_baud_rate():
    print("Please select a baud rate:")
    for idx, rate in enumerate(COMMON_BAUD_RATES, 1):
        print(f"{idx}. {rate}")
    while True:
        try:
            choice = int(input(f"Enter your choice (number, default is {DEFAULT_BAUD_RATE}): "))
            if 1 <= choice <= len(COMMON_BAUD_RATES):
                return COMMON_BAUD_RATES[choice - 1]
            else:
                print("Invalid choice. Please choose again.")
        except ValueError:
            return DEFAULT_BAUD_RATE

def user_input_universe():
    universes = []
    while True:
        try:
            uni = int(input("Enter universe number (or type 'done' to finish): "))
            universes.append(uni)
        except ValueError:
            if len(universes) > 0:
                break
            print("Please enter a valid universe number or 'done'.")

    return universes

def send_test_pattern(ser, num_leds=150):
    patterns = [
        [255, 0, 0],   # Red
        [0, 255, 0],   # Green
        [0, 0, 255],   # Blue
        [0, 0, 0]     # Off
    ]
    for pattern in patterns:
        adalight_data = b'Ada' + num_leds.to_bytes(2, byteorder='big', signed=False) + b'\x00'
        adalight_data += bytes(pattern) * num_leds
        ser.write(adalight_data)
        time.sleep(1)

def e131_to_adalight_bridge(chosen_port, baud_rate, universes):
    receiver = sacn.sACNreceiver()
    receiver.start()
    ser = serial.Serial(chosen_port, baud_rate)
    time.sleep(2)

    send_test_pattern(ser)

    def process_packet(packet):
        rgb_values = [packet.dmxData[i:i+3] for i in range(0, 150 * 3, 3)]  # assuming 150 LEDs
        adalight_data = b'Ada' + len(rgb_values).to_bytes(2, byteorder='big', signed=False) + b'\x00'
        for val in rgb_values:
            adalight_data += bytes(val)
        ser.write(adalight_data)

    for uni in universes:
        @receiver.listen_on('universe', universe=uni)
        def on_data(packet, universe=uni):  # using default argument to capture loop variable
            process_packet(packet)

    try:
        print(f"Listening on universe(s): {', '.join(map(str, universes))}")
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        pass

    ser.close()
    receiver.stop()

def print_config_summary(chosen_port, baud_rate, universes):
    print("\nSelected Configuration:")
    print("-----------------------")
    print(f"USB Device: {chosen_port}")
    print(f"Baud Rate: {baud_rate}")
    print(f"Listening on Universes: {', '.join(map(str, universes))}\n")
    input("Press Enter to start...")

def main():
    chosen_port = None
    baud_rate = None
    universes = []

    # Parse arguments
    args = sys.argv[1:]
    if args:
        chosen_port = args[0]
        if len(args) > 1:
            try:
                baud_rate = int(args[1])
            except ValueError:
                print("Invalid baud rate argument. Using default.")
                baud_rate = DEFAULT_BAUD_RATE
            universes = [int(u) for u in args[2:]]

    # If no device passed, ask the user to choose
    if not chosen_port:
        ports = list_serial_ports()
        if not ports:
            print("No USB devices found.")
            sys.exit(1)
        chosen_port = user_choice_port(ports)

    # If no baud rate passed as argument, ask the user to select
    if not baud_rate:
        baud_rate = select_baud_rate()

    # If no universes passed as arguments, ask the user to input
    if not universes:
        universes = user_input_universe()

    print_config_summary(chosen_port, baud_rate, universes)
    e131_to_adalight_bridge(chosen_port, baud_rate, universes)

if __name__ == "__main__":
    main()
